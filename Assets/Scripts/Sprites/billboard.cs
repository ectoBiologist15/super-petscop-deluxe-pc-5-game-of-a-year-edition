﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
#pragma warning disable IDE1006 // Naming Styles
public class billboard : MonoBehaviour
#pragma warning restore IDE1006 // Naming Styles
{

    Transform cameratrans;

    // Start is called before the first frame update
    void Start()
    {
        cameratrans = GameObject.Find("Main Camera").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localEulerAngles = new Vector3(cameratrans.rotation.eulerAngles.x, -cameratrans.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
    }
}
