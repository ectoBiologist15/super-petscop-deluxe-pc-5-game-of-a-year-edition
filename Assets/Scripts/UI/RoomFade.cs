﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RoomFade : MonoBehaviour
{

    //Simple and unoptimised and messy as fuck fading between rooms

    public bool white;
    Image box;
    Color desiredColor;
    public bool fadeOut;
    public float alpha;
    public bool fading;
    public bool changeScene;
    public int scene;
    public float speed;
    public Sprite loadingScreen;

    void Start()
    {

        //what is this mess

        fadeOut = false; //this part only happens in the menu where its 100% a fade in so
        alpha = 1; //same as above, if it's a fade in the alpha starts at 1
        changeScene = false; //just in case
        speed = 1;
        fading = true;
        box = GetComponentInChildren<Image>();

        if (SceneManager.GetActiveScene().name == "Menu") speed = 1;
        else speed = 2;
    }

    void Update()
    {
        /*basically this decides whether to make it white/black and still have the "fadeOut" alpha
        unity's fault for using 0-1 instead of 0-255 lol*/
        desiredColor = new Color(Convert.ToSingle(white), Convert.ToSingle(white), Convert.ToSingle(white), Convert.ToSingle(fadeOut));

        //todo: some funny shit to make it do proper fades, right now this is only used in the menu so it should be fine
        if (alpha != desiredColor.a)
        {
            //thanks mike
            alpha += fadeOut ? speed * Time.deltaTime : -speed * Time.deltaTime;
        }

        //and finally change the box alpha
        box.color = new Color(desiredColor.r, desiredColor.g, desiredColor.b, alpha);

        if (alpha == desiredColor.a) fading = false; else fading = true;

        //make sure it doesnt go too low or too high
        alpha = Mathf.Clamp(alpha, 0, 1);

        if (alpha != 1) box.gameObject.SetActive(fading);

        //Load the scene/fade in again
        if (alpha == 1 && fadeOut)
        {
            if (changeScene)
            {
                if(GameObject.Find("Player") != null) GameObject.Find("Player").SetActive(false); //STOP THE FUCKIGN FOOTSTEPS BEFORE I STOP YOUR BREATHING
                StartCoroutine(Wait(4)); //wait on loading screen for like four seconds
            }
            else
            {
                //do some funny shit to change room
                fadeOut = false;
                PlayerPrefs.SetFloat("RoomNumber", PlayerPrefs.GetFloat("RoomNumber") + 1);
            }
        }
    }

    IEnumerator Wait(float seconds)
    {
        yield return new WaitForSecondsRealtime(0.83333333333f);
        box.color = new Color(255, 255, 255, 255); //set colour to white so that the loading screen isnt tinted
        box.sprite = loadingScreen;
        yield return new WaitForSecondsRealtime(seconds);
        SceneManager.LoadScene(scene); //fade object in new scene will do some funnies to set it to the right room
    }
}