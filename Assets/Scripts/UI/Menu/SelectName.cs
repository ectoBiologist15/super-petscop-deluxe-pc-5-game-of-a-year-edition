﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SelectName : MonoBehaviour
{

    //Select characters for the name

    //long arse 2d array containing every character that can be entered
    public string[,] characters = new string[,] { { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M" },{ "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },{ "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m" },{ "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" },{ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", " ", ".", "!" },{ "?", ",", ";", ":", "\"", "'", "(", ")", "/", "-", "+", "backspace", " " } };

    public int x;
    public int y;
    public string enteredText;
    TMPro.TMP_Text text;
    RectTransform trans;
    RectTransform boxTransform;
    RectTransform darkBoxTransform;
    AudioSource source;
    AudioSource source2;
    public AudioClip clip;
    RoomFade roomFade;

    // Start is called before the first frame update
    void Start()
    {
        trans = GetComponent<RectTransform>();
        boxTransform = GameObject.Find("Name box").GetComponent<RectTransform>();
        darkBoxTransform = GameObject.Find("Selected name box").GetComponent<RectTransform>();
        text = GameObject.Find("BoxText").GetComponent<TMPro.TMP_Text>();
        source = GetComponent<AudioSource>();
        //cant do get component with both
        source2 = gameObject.AddComponent<AudioSource>();
        roomFade = GameObject.Find("Fade").GetComponent<RoomFade>();
    }

    // Update is called once per frame
    void Update()
    {
        //add to/remove from string
        if(Input.GetKeyDown(KeyCode.Z))
        {
            if (characters[y, x] != "backspace")
            {
                if (text.renderedWidth < 128) enteredText += characters[y, x];
            }
            else enteredText = enteredText.Substring(0, enteredText.Length - 1);
        }

        //x axis
        if (Input.GetKeyDown(KeyCode.LeftArrow) && x > 0)
        {
            --x;
            source.Play();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && x < 12)
        {
            ++x;
            source.Play();
        }

        //y axis
        if (Input.GetKeyDown(KeyCode.UpArrow) && y > 0)
        {
            --y;
            source.Play();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && y < 5)
        {
            ++y;
            source.Play();
        }

        //set position
        trans.localPosition = new Vector3(-96 + (x * 16), 38 - (y * 20));
        darkBoxTransform.position = boxTransform.position;

        text.text = enteredText;

        //Finish text
        if(Input.GetKeyDown(KeyCode.Return) && !String.IsNullOrWhiteSpace(enteredText))
        {
            /*Save player name to registry.
             * Only one save, might change later
             * Thanks mike for telling me about PlayerPrefs*/
            PlayerPrefs.SetString("PlayerName", enteredText);
            source2.clip = clip;
            source2.Play();
            roomFade.changeScene = true;
            roomFade.scene = 2; //sans room is 0, menu is 1
            roomFade.fadeOut = true;
        }
    }
}
