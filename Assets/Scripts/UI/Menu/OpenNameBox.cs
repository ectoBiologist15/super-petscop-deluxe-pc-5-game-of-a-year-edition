﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class OpenNameBox : MonoBehaviour
{

    Animator nameBoxAnimator;
    Animator keyboardAnimator;
    bool enterPressed;
    AudioSource source;
    public AudioClip clip;
    RoomFade roomFade;

    // Start is called before the first frame update
    void Start()
    {
        nameBoxAnimator = GameObject.Find("Name box").GetComponent<Animator>();
        keyboardAnimator = GetComponent<Animator>();
        enterPressed = false;
        source = GetComponent<AudioSource>();
        roomFade = GameObject.Find("Fade").GetComponent<RoomFade>();

        //avoids anything from changing before it appears
        GameObject.Find("Mask").GetComponent<SelectName>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Check for enter ONCE
        if(Input.GetKeyDown(KeyCode.Return) && !enterPressed)
        {
            enterPressed = true;

            //now that it's no longer completely off the screen we can use it again
            GameObject.Find("Mask").GetComponent<SelectName>().enabled = true;

            /*Check if the player name has already been set
             * If it has, skip this and load the scene that the player is in
             * If it isn't open the enter name box*/

            if(PlayerPrefs.HasKey("PlayerName"))
            {
                /*this might work but i havent finished roomfade yet:*/
                source.clip = clip;
                source.Play();
                roomFade.changeScene = true;
                if (!PlayerPrefs.HasKey("SceneIndex")) roomFade.scene = 2; //sans room is 0, menu is 1
                else roomFade.scene = PlayerPrefs.GetInt("SceneIndex");
                roomFade.fadeOut = true;
            } else
            {
                //Play animation and sound
                nameBoxAnimator.Play("Move left");
                keyboardAnimator.Play("Move left");
                source.clip = clip;
                source.Play();
            }
        }

    }
    private void LateUpdate()
    {
        if (roomFade.fadeOut)
        {
            GameObject.Find("Main Camera").GetComponent<AudioSource>().volume -= 0.005f;
        }
    }
}
