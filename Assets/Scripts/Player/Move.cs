﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Move : MonoBehaviour
{
    public int dirint;
    SpriteRenderer sprite;
    public Sprite[] sprites;
    Animator anim;
    public AnimationClip[] animclip;
    public float xspeed;
    public float yspeed;
#pragma warning disable CS0108 //fuck off warnings
    Rigidbody rigidbody;
#pragma warning restore CS0108

    // Start is called before the first frame update
    void Start()
    {
        xspeed = 0;
        sprite = GetComponentInChildren<SpriteRenderer>();
        anim = GetComponentInChildren<Animator>();
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //Animation

        //get input and set direction
        if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow))
        {
            dirint = 0;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow))
        {
            dirint = 1;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            dirint = 2;
        }
        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.DownArrow))
        {
            dirint = 3;
        }
    }

    private void LateUpdate()
    {
        //More animation

        //if no keys are pressed set it to the right direction
        if (
            !Input.GetKey(KeyCode.LeftArrow) &&
            !Input.GetKey(KeyCode.RightArrow) &&
            !Input.GetKey(KeyCode.UpArrow) &&
            !Input.GetKey(KeyCode.DownArrow)
        )
        {
            anim.enabled = false;
            sprite.sprite = sprites[dirint]; //set the sprite, theres no need to do an else its jsut too much effort
        }

        if (
            Input.GetKey(KeyCode.LeftArrow) ||
            Input.GetKey(KeyCode.RightArrow) ||
            Input.GetKey(KeyCode.UpArrow) ||
            Input.GetKey(KeyCode.DownArrow)
        )
        {
            if (anim.enabled == false)
            {
                anim.enabled = true; //LITERALLY RIPPED FROM M3D LMFAO
                anim.Play(animclip[dirint].name, -1, 0);
            }
        }

        //if ANYTHING gets released and its not already playing the right thing then disable
        if (
            Input.GetKeyUp(KeyCode.LeftArrow) ||
            Input.GetKeyUp(KeyCode.RightArrow) ||
            Input.GetKeyUp(KeyCode.UpArrow) ||
            Input.GetKeyUp(KeyCode.DownArrow)
        ) if(!anim.GetCurrentAnimatorStateInfo(0).IsName(animclip[dirint].name)) anim.enabled = false; //if the animation name isnt what it should be disable it, it gets enabled next frame anyway

        //Movement

        //Get keypresses and set speed accordingly
        if (Input.GetKey(KeyCode.LeftArrow) && xspeed != 1) xspeed += 0.04f;
        if (Input.GetKey(KeyCode.RightArrow) && xspeed != -1) xspeed -= 0.04f;

        if (Input.GetKey(KeyCode.UpArrow) && yspeed != -1) yspeed -= 0.04f;
        if (Input.GetKey(KeyCode.DownArrow) && yspeed != 1) yspeed += 0.04f;


        //Slow down

        //X
         if (!Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow) && xspeed < 0)
        {
            xspeed += 0.04f;
            if (xspeed > 0) xspeed = 0;
        } else if(!Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow) &&  xspeed > 0)
        {
            xspeed -= 0.04f;
            if (xspeed < 0) xspeed = 0;
        }

        //Y
        if (!Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow) && yspeed < 0)
        {
            yspeed += 0.04f;
            if (yspeed > 0) yspeed = 0;
        } else if (!Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow) && yspeed > 0)
        {
            yspeed -= 0.04f;
            if (yspeed < 0) yspeed = 0;
        }

        //Clamp it to make sure it doesnt fuck up because float
        xspeed = Mathf.Clamp(xspeed, -1, 1); //clamp it because otherwise it kinda breaks

        yspeed = Mathf.Clamp(yspeed, -1, 1);

        //Set the velocity of the rigidbody
        rigidbody.velocity = new Vector3(Mathf.Clamp(xspeed, -1, 1) * 3, 0, Mathf.Clamp(yspeed, -1, 1) * 3);
    }
}