﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoTile : MonoBehaviour
{
    /*stops the material stretching with the object
     * basically means whatever the size of the object the size of each "tile" will stay the same
     * tiling doesn't work properly with uv maps tho*/

    public float x;
    public float y;
    Renderer render;

    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        render.material.mainTextureScale = new Vector2(transform.lossyScale.x * x, transform.lossyScale.y * y);
    }
}
