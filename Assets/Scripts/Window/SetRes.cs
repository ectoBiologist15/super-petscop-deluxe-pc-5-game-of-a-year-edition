﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetRes : MonoBehaviour
{

    //this script should only be in one scene, if it's anywhere else there's a problem
    //Forces the resolution and loads the menu

    // Start is called before the first frame update
    void Start()
    {
        Screen.SetResolution(960, 720, false);
        SceneManager.LoadScene(1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
