﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Follow : MonoBehaviour
{

    /// <summary>
    /// Follow the player around. The camera has limits to stop it from moving off the screen, or too far to one direction.
    /// This will probably be changed in future for situations like this: https://www.youtube.com/watch?v=teJDdkWHAdw&t=2m32s
    /// But for now it should do.
    /// </summary>

    public Transform playertrans;
    public float xmin;
    public float xmax;
    public float ymin;
    public float ymax;

    // Start is called before the first frame update
    void Start()
    {
        playertrans = GameObject.Find("Player").GetComponent<Transform>();
        //Get the scene and set limits accordingly
        switch (SceneManager.GetActiveScene().name)
        {
            case "Gift Plane":
                xmin = 0;
                xmax = -6;
                ymin = 7.5f;
                ymax = 7.5f;
                return;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Set cam transform to player transform
        transform.position = new Vector3(playertrans.position.x, transform.position.y /*camera should never go up/down*/, playertrans.position.z);
        
        //Add limits
        //X
        if(xmin != 0)
        {
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, xmin, 99999999 /*i am bad*/), transform.position.y, transform.position.z);
        }
        if(xmax != 0)
        {
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, -99999999, xmax), transform.position.y, transform.position.z);
        }

        //IM LAZY AND THIS IS UNOPTIMISED AND MESSY HELP ME

        //Y
        if (ymin != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Clamp(transform.position.y, ymin, 99999999));
        }
        if (ymax != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Clamp(transform.position.z, -99999999, ymax));
        }
    }
}
