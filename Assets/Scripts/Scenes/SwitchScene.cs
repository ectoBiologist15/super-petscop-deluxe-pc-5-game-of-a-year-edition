﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour
{

    RoomFade rfade;
    Move move;

    // Start is called before the first frame update
    void Start()
    {
        //get the room fade script on the object
        rfade = GameObject.Find("Fade").GetComponent<RoomFade>();

        //get the move script from the player
        move = GameObject.Find("Player").GetComponent<Move>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //is it the player???
        if (other.gameObject.CompareTag("Player"))
        {
            //disable the players movement
            move.enabled = false;

            //set shit on the script so it fades
            rfade.changeScene = true;
            rfade.scene = 1; //temp, sends to menu
            rfade.fadeOut = true;
        }
    }

    private void LateUpdate()
    {
        //fade out the music
        if (rfade.changeScene == true) GameObject.Find("Main Camera").GetComponent<AudioSource>().volume -= 0.005f;
    }
}
